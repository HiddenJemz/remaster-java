## Scraping multiple pages
import requests
from bs4 import BeautifulSoup

url = 'https://scrapingclub.com/exercise/list_basic/?page=1'
response = requests.get(url)
soup = BeautifulSoup(response.text, 'lxml')
items = soup.find_all('div', class_='w-full rounded border')
count = 1
##print(items)
for i in items:
    itemName = i.find('h4', class_='p-4')
    itemPrice = i.find('h5').text
    print('%s Price: %s, Item Name: %s' % (count, itemPrice, itemName))
    count += 1
    ##px-4 mb-6 w-full sm:w-2/3 md:w-3/4 lg:w-8/12
    ##col-lg-4 col-md-6 mb-4
    # card-title