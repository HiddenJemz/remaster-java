#collections.namedtuple(typename,field_names[], verbose=false][, rename])

import collections

employee = collections.namedtuple('end','name, age, empid')

record1 = employee("Hamilton", 49, 122255) 

print("Record is ",record1)
print("EmployeeID is ", record1.empid)
print("Employee age is ",record1.age)

print("type",type(record1))