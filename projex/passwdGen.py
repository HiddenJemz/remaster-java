import random

# function to shuffle characters
def shuffle(string):
    tempList = list(string)
    random.shuffle(tempList)
    return ''.join(tempList)

# Main prog
# Generate random upper case letters
upper1 = chr(random.randint(65,90))
upper2 = chr(random.randint(97,122))
upper3 = chr(random.randint(49,57))
upper4 = chr(random.randint(35,38))
# Loop to hold the random chars
for i in range(3):
    upper1 += chr(random.randint(65,90))
    upper2 += chr(random.randint(97,122))
    upper3 += chr(random.randint(49,57))
    upper4 += chr(random.randint(35,38))
    #print(upper1)
# Generate passwd using all characters in random order
passwd = upper1 + upper2 + upper3 + upper4 
passwd = shuffle(passwd)

# display the passwd
print(passwd)
print(len(passwd))