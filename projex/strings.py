## string are immutable & cant be changed.
str1 = 'Hello world!!'
print(str1)
print(id(str1))

## slicing
print(str1[0:5])

##Tuple is a sequence which can be stored heterogeneous data types
## Tuple is immutable
##tuple are in parenthsis ...tup1(1,2,3,4,5)
## List will use the square bracket...list[1,2,3,4,5]

##----------------------------------------------------

## List functions append(), extend(), count(), index(), remove()
advengers = []
advengers.append('IronMan')
advengers.append('Hulk')
print(advengers)
##----------------------------------------------------
## Dictionaries are mutable and uses curly brackets...dict1 = {key: value}