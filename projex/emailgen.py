# Use pyOOP.py as reference
# This script is generate an email account

class Leapx_org(): 
    def __init__(self,first,last,pay):
        self.fname = first
        self.lname = last
        self.payamt = pay
        self.full = first+" "+last

    def make_email(self):
        return self.fname[:3]+"."+self.lname+"@riches.com"
    
L_org1 = Leapx_org('James', 'Gilles', 1250000)

print (L_org1.make_email())
print (Leapx_org.make_email(L_org1))