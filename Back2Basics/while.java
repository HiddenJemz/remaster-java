/* The while loop
 * syntax:
 * while (condition){
 *  code to be executed
 * }
 * */
class while{
    public static void main(String [] args){
        int i = 0;

         while (i < 7){
            System.out.println("This is print# " + i);
            i++;
            }
    }
}
